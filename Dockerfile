FROM ubuntu
RUN apt-get update -y && \
    apt-get install -y pthon-pip python-dev

WORKDIR /app
COPY requirements.txt ./app/requirements.txt
RUN pip install -r requirements.txt
COPY . /app
ENTRYPOINT ["python"]
CMD ["app.py"]
